CREATE DATABASE  IF NOT EXISTS `demoshop` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `demoshop`;
-- MySQL dump 10.13  Distrib 5.7.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: demoshop
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `goodsdata`
--

DROP TABLE IF EXISTS `goodsdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goodsdata` (
  `id` int(11) NOT NULL,
  `imageUrl` varchar(100) DEFAULT NULL,
  `title` varchar(45) CHARACTER SET utf8 NOT NULL,
  `price` float NOT NULL,
  `checked` tinyint(4) NOT NULL,
  `loaded` varchar(45) NOT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goodsdata`
--

LOCK TABLES `goodsdata` WRITE;
/*!40000 ALTER TABLE `goodsdata` DISABLE KEYS */;
INSERT INTO `goodsdata` VALUES (1,'http://localhost:8081/SSM_war_exploded/image/good1.jpg','西瓜啵啵',20.5,0,'loaded',1),(2,'http://localhost:8081/SSM_war_exploded/image/good2.jpg','大红袍牛乳奶茶',11,0,'loaded',1),(3,'http://localhost:8081/SSM_war_exploded/image/good3.jpg','草莓多肉芝士茶',21,0,'loaded',1),(4,'http://localhost:8081/SSM_war_exploded/image/good4.jpg','大师咖啡',17,0,'loaded',1),(5,'http://localhost:8081/SSM_war_exploded/image/good5.jpg','超级水果茶',19,0,'loaded',1);
/*!40000 ALTER TABLE `goodsdata` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-30 18:34:07
