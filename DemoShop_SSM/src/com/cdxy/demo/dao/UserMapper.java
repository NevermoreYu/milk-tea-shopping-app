package com.cdxy.demo.dao;

import com.cdxy.demo.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserMapper {

    User login(String username, String password);

    User findUserByUsername(String username);

    void register(Long id,String username,String password,
                  Integer money,String gender,String imageUrl);

    User updateImage(String imageUrl,Long id);


}
