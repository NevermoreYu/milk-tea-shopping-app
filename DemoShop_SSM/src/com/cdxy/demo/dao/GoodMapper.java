package com.cdxy.demo.dao;

import com.cdxy.demo.pojo.GoodsData;


public interface GoodMapper {

    GoodsData findGoodsById(Integer id);

    void addGoods(Integer id, String imageUrl, String title, float price);

    Long updateGoods(GoodsData goodsData);

    int countNum ();

    boolean deleteGoods(Integer id);

}
