package com.cdxy.demo.pojo;

import sun.plugin2.util.ColorUtil;
import sun.security.util.SecurityConstants;

import java.awt.*;

public class SwiperData {
    private String imageUrl;

    public SwiperData() {
    }

    public SwiperData(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
