package com.cdxy.demo.pojo;

public class GoodsData {
    private Integer id;
    private String imageUrl;
    private String title;
    private Float price;
    private Boolean checked;
    private String loaded;
    private Integer number;


    public GoodsData() {
    }

    public GoodsData(Integer id, String imageUrl, String title, Float price, Boolean checked, String loaded, Integer number) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.title = title;
        this.price = price;
        this.checked = checked;
        this.loaded = loaded;
        this.number = number;
    }

//    public GoodsData(Integer id, String imageUrl, String title, Float price) {
//        this.id = id;
//        this.imageUrl = imageUrl;
//        this.title = title;
//        this.price = price;
//    }


    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getLoaded() {
        return loaded;
    }

    public void setLoaded(String loaded) {
        this.loaded = loaded;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "GoodsData{" +
                "id=" + id +
                ", imageUrl='" + imageUrl + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                '}';
    }
}
