package com.cdxy.demo.pojo;

public class ListData {
    private String title;

    public ListData() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ListData(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "ListData{" +
                "title='" + title + '\'' +
                '}';
    }
}
