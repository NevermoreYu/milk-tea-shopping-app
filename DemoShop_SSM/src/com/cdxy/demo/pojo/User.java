package com.cdxy.demo.pojo;

public class User {
    private Long id;
    private String username;
    private String password;
    private Integer money;
    private String gender;
    private String imageUrl;

    public User(){

    }

    public User(Long id) {
        this.id = id;
    }

    public User(Long id, String username, String password, Integer money, String gender, String imageUrl) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.money = money;
        this.gender = gender;
        this.imageUrl = imageUrl;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", money=" + money +
                ", gender=" + gender +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
