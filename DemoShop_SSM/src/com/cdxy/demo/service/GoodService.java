package com.cdxy.demo.service;

import com.cdxy.demo.dao.GoodMapper;
import com.cdxy.demo.pojo.GoodsData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodService {

    @Autowired
    private GoodMapper goodMapper;

    public GoodsData getGoods(Integer id){
        return goodMapper.findGoodsById(id);
    }

    public void addGoods(Integer id,String imageUrl,String title,float price){
        goodMapper.addGoods(id,imageUrl,title,price);
        return ;
    }

    public int countNum(){
        return goodMapper.countNum();
    }
}
