package com.cdxy.demo.service;

import com.cdxy.demo.dao.UserMapper;
import com.cdxy.demo.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public User login(String username,String password){
        return userMapper.login(username,password);
    }

    public User findUserByUsername(String username){
        return userMapper.findUserByUsername(username);
    }

    public void register(Long id,String username,String password){
        userMapper.register(id,username,password,0,"未修改","http://localhost:8081/SSM_war_exploded/image/missing-face.png");
        return ;
    }

    public User updateImage(String imageUrl,Long id){
        return userMapper.updateImage(imageUrl,id);
    }

}
