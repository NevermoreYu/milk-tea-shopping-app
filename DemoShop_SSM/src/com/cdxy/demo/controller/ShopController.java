package com.cdxy.demo.controller;

import com.cdxy.demo.pojo.*;
import com.cdxy.demo.service.GoodService;
import com.cdxy.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.*;

@Controller
public class ShopController {

    @Autowired
    private GoodService goodService;
    @Autowired
    private UserService userService;

//   轮播图展示
    @RequestMapping("/swiper")
    @ResponseBody
    public Data<SwiperData> getSwiperData(){

        List<SwiperData> list = new ArrayList<>();
        for (int i=1;i<=3;i++){
            list.add(new SwiperData(goodService.getGoods(i).getImageUrl()));
        }

        Data<SwiperData> data = new Data<>(200, "ok", list);
        return data;
    }
//   推荐商品页
    @RequestMapping(value = "/Goods",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public Data<GoodsData> getGoodsData(){

        List<GoodsData> list = new ArrayList<>();
        for (int i=1; i<=goodService.countNum(); i++){
            list.add(goodService.getGoods(i));
        }

        Data<GoodsData> data = new Data<>(200,"ok",list);
        return data;
    }

//   商品详情页
    @RequestMapping(value = "/GoodsById",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public Data<GoodsData> getGoodsDataById(Integer id){

        List<GoodsData> list = new ArrayList<>();

        list.add(goodService.getGoods(id));

        Data<GoodsData> data = new Data<>(200,"ok",list);
        return data;
    }

//   垂直导航分类
    @RequestMapping(value = "/List",produces = {"application/json;charset=utf-8"})
    @ResponseBody
    public Data<ListData> getListData(){

        ListData l1 = new ListData("人气Top");
        ListData l2 = new ListData("瑞纳冰");
        ListData l3 = new ListData("大师咖啡");
        ListData l4 = new ListData("经典拿铁");
        ListData l5 = new ListData("进口咖啡");
        ListData l6 = new ListData("NFC果汁");
        ListData l7 = new ListData("甄选酸奶");
        ListData l8 = new ListData("小鹿茶精选");

        List<ListData> list = new ArrayList<>();
        list.add(l1);
        list.add(l2);
        list.add(l3);
        list.add(l4);
        list.add(l5);
        list.add(l6);
        list.add(l7);
        list.add(l8);



        Data<ListData> data = new Data<>(200, "ok", list);
        return data;
    }

//   登录
    @ResponseBody
    @RequestMapping("/login")
    public Data<User> login(String username,String password) {
        if (userService.login(username,password)!=null){
            List<User> list = new ArrayList<>();
            list.add(userService.login(username,password));

            return new Data<User>(200,"login",list);

        }else{
            return new Data<User>(200,"error",null);
        }
    }
//   注册
    @ResponseBody
    @RequestMapping("/register")
    public Data<User> register(Long id,String username,String password){
        if (userService.findUserByUsername(username)!=null){

            return new Data<User>(200,"already_login",null);

        }else{
            List<User> list = new ArrayList<>();

            userService.register(id,username,password);
            list.add(userService.login(username,password));
            return new Data<User>(200,"register",list);
        }

    }


//   上传头像
    @RequestMapping("/upload")
    @ResponseBody
    public Data<User> upload(Long id, MultipartFile file, HttpServletRequest request)throws Exception{
        String uploadPath = request.getServletContext().getRealPath("/image");

        File dir = new File(uploadPath);
        if (!dir.exists()){
            dir.mkdirs();
        }
        String originalFilename = file.getOriginalFilename();
        String suffix = originalFilename.split("\\.")[1];
        String uuid = UUID.randomUUID().toString();
        String newFileName = uuid+"."+suffix;
        file.transferTo(new File(uploadPath+File.separator+newFileName));

        userService.updateImage("http://localhost:8081/SSM_war_exploded/image/good2.jpg",id);
        List<User> list = new ArrayList<>();
        list.add( userService.updateImage("http://localhost:8081/SSM_war_exploded/image/good2.jpg",id));

        return new Data<User>(200,"uploadSuccess",list);
    }
}
