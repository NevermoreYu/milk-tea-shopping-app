<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <c:if test="${empty goodsData}">
        没数据
    </c:if>
    <c:if test="${!empty goodsData}">
        <table border="1" cellspacing="0" cellpadding="2">
            <tr>
                <th>学号</th>
                <th>姓名</th>
                <th>年龄</th>
                <th>班级</th>
            </tr>
            <c:forEach items="${goodsData}" var="goodsData">
                <tr>
                    <td>${goodsData.id}</td>
                    <td>${goodsData.imageUrl}</td>
                    <%--<td>${goodsData.title}</td>--%>
                    <%--<td>${student.clazz.clazzName}</td>--%>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</body>
</html>
